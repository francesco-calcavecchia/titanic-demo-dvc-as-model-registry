# titanic-model
Titanic models


## TODO: how to go on after creating the package
- add the required packages to `setup.cfg` in the field `install_requires`
- complete filling the setup.cfg with the optional information commented out
- the built python package will be pushed by default to the GitLab pypi repository. If you want to change this behaviour, modify the task `deploy package to gitlab pypi` in the file `.gitlab-ci.yml`.


## For the developer


* Create a virtual environment and activate it (if you have [pyenv](https://github.com/pyenv/pyenv) installed the 
  version of python will be automatically set, otherwise refer to the file `.python-version`)
  ```shell
  python -m venv venv
  source venv/bin/activate
  ```

* Install the developer dependencies you will need
  ```shell
  pip install -U pip wheel setuptools
  pip install -e .[dev]
  ```
  
* Set black as pre-commit package (will automatically apply [black](https://github.com/psf/black) before committing)
  ```shell
  pre-commit install
  ```
  
* To run the tests
  ```shell
  python -m unittest discover test
  ```
  