from pathlib import Path

import cloudpickle
import fire
import pandas as pd

from titanic_model.feature.stack import social_status
from titanic_model.linear import LinearModel


def train(data: Path, model_path: Path):
    df = pd.read_csv(data)
    model = LinearModel(feature_stack=social_status)
    model.fit(df=df, target_col="Survived")
    with open(model_path, "wb") as f:
        cloudpickle.dump(model, f)


def main():
    fire.Fire(train)
