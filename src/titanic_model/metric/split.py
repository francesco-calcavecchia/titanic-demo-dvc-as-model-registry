from pathlib import Path

import fire
import pandas as pd
from sklearn.model_selection import train_test_split


def split_train_test_set(source_data: Path, train: Path, test: Path):
    df = pd.read_csv(source_data)
    train_df, test_df = train_test_split(df, test_size=0.2, random_state=42)
    train_df.to_csv(train)
    test_df.to_csv(test)


def main():
    fire.Fire(split_train_test_set)
