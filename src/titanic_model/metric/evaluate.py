import json
from pathlib import Path
from typing import Tuple

import cloudpickle
import fire
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import classification_report, roc_curve, auc

from titanic_model.linear import LinearModel


def evaluate(test_data: Path, model_path: Path, report_path: Path, roc_fig_path: Path, fpr_and_tpr_path: Path):
    with open(model_path, "rb") as f:
        model: LinearModel = cloudpickle.load(f)
    df = pd.read_csv(test_data)

    predictions = model.predict(df=df)
    report_dict = classification_report(y_true=df["Survived"], y_pred=predictions, output_dict=True)

    fpr, tpr, roc_auc = produce_roc_curve_values(model=model, test_df=df)
    report_dict["ROC AUC"] = roc_auc

    with open(report_path, "w") as f:
        json.dump(report_dict, f)

    write_tpr_and_tpr(fpr=fpr, tpr=tpr, file_path=fpr_and_tpr_path)
    plot_roc_curve(fpr=fpr, tpr=tpr, roc_auc=roc_auc, fig_path=roc_fig_path)


def produce_roc_curve_values(model, test_df: pd.DataFrame) -> Tuple[np.array, np.array, float]:
    y_score = model.predict_prob_surviving(df=test_df)

    fpr, tpr, _ = roc_curve(test_df["Survived"], y_score)
    roc_auc = auc(fpr, tpr)

    return fpr, tpr, roc_auc


def write_tpr_and_tpr(fpr: np.array, tpr: np.array, file_path: Path) -> None:
    pd.DataFrame({"false_positive_rate": fpr, "true_positive_rate": tpr}).to_csv(file_path, index=False)


def plot_roc_curve(fpr: np.array, tpr: np.array, roc_auc: float, fig_path: Path) -> None:
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color="darkorange", lw=lw, label="ROC curve (area = %0.3f)" % roc_auc)
    plt.plot([0, 1], [0, 1], color="navy", lw=lw, linestyle="--")
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("Receiver operating characteristic example")
    plt.legend(loc="lower right")
    plt.savefig(fig_path)


def main():
    fire.Fire(evaluate)


if __name__ == "__main__":
    main()
