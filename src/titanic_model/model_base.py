from abc import ABC, abstractmethod

import pandas as pd


class ModelBase(ABC):
    @abstractmethod
    def fit(self, df: pd.DataFrame, target_col: str):
        pass

    @abstractmethod
    def predict(self, df: pd.DataFrame) -> pd.Series:
        pass

    @abstractmethod
    def predict_prob_surviving(self, df: pd.DataFrame) -> pd.Series:
        pass
