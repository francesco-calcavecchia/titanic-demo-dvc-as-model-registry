import pandas as pd
from sklearn.linear_model import LogisticRegression

from titanic_model.feature.stack import FittableStack
from titanic_model.model_base import ModelBase


class LinearModel(ModelBase):
    def __init__(self, feature_stack: FittableStack):
        self._sklearn_model = LogisticRegression()
        self._feature_stack = feature_stack

    def fit(self, df: pd.DataFrame, target_col: str):
        self._feature_stack.fit(df=df)
        enriched_df = (
            self._feature_stack.with_dependencies()
            .insert_into(df=df)
            .filter(items=self._feature_stack.names() + [target_col])
        )
        enriched_df = enriched_df.dropna()
        self._sklearn_model = self._sklearn_model.fit(X=enriched_df.drop(columns=target_col), y=enriched_df[target_col])

    def predict(self, df: pd.DataFrame) -> pd.Series:
        return self._sklearn_model.predict(X=self._prepare_data_for_prediction(df=df))

    def predict_prob_surviving(self, df: pd.DataFrame) -> pd.Series:
        return self._sklearn_model.predict_proba(X=self._prepare_data_for_prediction(df=df))[:, 1]

    def _prepare_data_for_prediction(self, df: pd.DataFrame) -> pd.DataFrame:
        return self._feature_stack.with_dependencies().insert_into(df=df).filter(items=self._feature_stack.names())
