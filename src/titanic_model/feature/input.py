from features_factory.input_features import IntInputFeature, StringInputFeature, FloatInputFeature

ticket_class = IntInputFeature(name="Pclass")
sex = StringInputFeature(name="Sex")
age = FloatInputFeature(name="Age", allow_nan=True)
num_siblings_spouses = IntInputFeature(name="SibSp")
num_parent_children = IntInputFeature(name="Parch")
fare = FloatInputFeature(name="Fare")
port_of_embarkation = StringInputFeature(name="Embarked", allow_nan=True)
