import pandas as pd
from features_factory.stack import Stack

from titanic_model.feature.encoded import encoded_sex
from titanic_model.feature.input import ticket_class, num_siblings_spouses, num_parent_children, fare


class FittableStack(Stack):
    def fit(self, df: pd.DataFrame):
        for feat in self:
            try:
                feat.fit(df=df)
            except AttributeError:
                pass


class_and_gender = FittableStack([ticket_class, encoded_sex])
social_status = FittableStack([ticket_class, num_siblings_spouses, num_parent_children, fare, encoded_sex])
