from typing import Tuple

import pandas as pd
from features_factory.feature_archetype import FeatureArchetype
from features_factory.generated_features import GeneratedFeature
from sklearn.preprocessing import LabelEncoder

from titanic_model.feature.input import sex, port_of_embarkation


class EncodedLabel(GeneratedFeature):
    def __init__(self, name: str, label_feature: FeatureArchetype):
        super().__init__(name=name)
        self._label_feature = label_feature
        self._le = LabelEncoder()

    def get_dependencies(self, **kwargs) -> Tuple:
        return (self._label_feature,)

    def fit(self, df: pd.DataFrame):
        self._le.fit(df[self._label_feature.name()])

    def generate(self, df: pd.DataFrame, **kwargs) -> pd.Series:
        return pd.Series(self._le.transform(df[self._label_feature.name()]))


encoded_sex = EncodedLabel(name="encoded_sex", label_feature=sex)
encoded_port_of_embarkation = EncodedLabel(name="encoded_port_of_embarkation", label_feature=port_of_embarkation)
